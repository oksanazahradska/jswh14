const btnDark = document.getElementById('dark');
const cssDark = document.getElementById('dark-css');
const DARK_CSS = './css/styledark.css'
const CSS = './css/style.css'

let isDarkmode = false;


const lastTheme = localStorage.getItem('theme') ?? 'light'

if (lastTheme =='light'){

cssDark.setAttribute('href', CSS)

}
else {
    cssDark.setAttribute('href', DARK_CSS)

}



btnDark.addEventListener('click', ()=>{
    if(isDarkmode) {
        cssDark.setAttribute('href', DARK_CSS);
        localStorage.setItem('theme', 'dark');
        isDarkmode = false;

    }
    else {
    cssDark.setAttribute('href', CSS);
    localStorage.setItem('theme', 'light');
isDarkmode = true; }




})
